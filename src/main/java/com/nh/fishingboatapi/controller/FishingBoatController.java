package com.nh.fishingboatapi.controller;

import com.nh.fishingboatapi.model.FishingBoatRequest;
import com.nh.fishingboatapi.service.FishingBoatService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/fishingboat")
public class FishingBoatController {
    private final FishingBoatService fishingBoatService;

    @PostMapping("/new")
    public String setFishingBoat(@RequestBody FishingBoatRequest request) {
        fishingBoatService.setFishingBoat(request);
        return "OK";
    }
}
