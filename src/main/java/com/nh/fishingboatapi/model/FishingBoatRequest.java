package com.nh.fishingboatapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FishingBoatRequest {
    private String name;
    private Byte no;
    private String captain;
}
